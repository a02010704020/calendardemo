

import Foundation
import UIKit

@IBDesignable class OPogressView:UIView{
    struct  Constant {
      
        static let lineWidth:CGFloat = 3
      
        static let trackColor = UIColor(red: 245/255, green: 245/255, blue: 245/255, alpha: 1)
        
        static var progressColor = UIColor.blue
    }
    
    var oletime:Double = 0
    
    var reColor = UIColor.blue.cgColor
   
    let trackLayer = CAShapeLayer()
    
    let progressLayer = CAShapeLayer()
   
    let path = UIBezierPath()
    
   
    var dot:UIView!
    let arc = CAShapeLayer()
    
    var dot_Stast:UIView!
    let dot_Arc = CAShapeLayer()
   
    var progressCenter:CGPoint {
        get{
            return CGPoint(x: bounds.midX, y: bounds.midY)
        }
    }
    
    
    var radius:CGFloat{
        get{
            return bounds.size.width/2 - Constant.lineWidth
        }
    }
    
    
    @IBInspectable var progress:Int = 0
    {
        willSet{

        }
        didSet{
            if progress >= 100 {
                progress = 100
                progressLayer.strokeColor = UIColor.orange.cgColor
          
                progressLayer.isHidden = false
               
            }else if progress <= 0 {
                progressLayer.strokeColor = UIColor.blue.cgColor
              
                progress = 0
                progressLayer.isHidden = true
               
            }else if progress > 0 {
                progressLayer.strokeColor = UIColor.blue.cgColor
              
                progressLayer.isHidden = false
               
            }
        }
    }
    
    required init?(coder aDecoder:NSCoder) {
        super.init(coder: aDecoder)
    }
    override init(frame:CGRect){
        super.init(frame: frame)
    }
    
    override func draw(_ rect: CGRect) {
       
        path.addArc(withCenter: CGPoint(x: bounds.midX, y: bounds.midY), radius: bounds.size.width/2 - Constant.lineWidth, startAngle: angleToRadian(-90), endAngle: angleToRadian(270), clockwise: true)
     
        trackLayer.frame = bounds
        trackLayer.fillColor = UIColor.clear.cgColor
        trackLayer.strokeColor = Constant.trackColor.cgColor
        trackLayer.lineWidth = Constant.lineWidth
        trackLayer.path = path.cgPath
        layer.addSublayer(trackLayer)
      
        progressLayer.frame = bounds
        progressLayer.fillColor = UIColor.clear.cgColor
        progressLayer.strokeColor = reColor
        progressLayer.lineWidth = Constant.lineWidth
        progressLayer.path = path.cgPath
       
        progressLayer.lineCap = CAShapeLayerLineCap.round
        progressLayer.strokeStart = 0
        progressLayer.strokeEnd = CGFloat(progress)/100.0
        layer.addSublayer(progressLayer)
        
        
        
        
      
    }
    
  
    func setProgress(_ pro:Int,animated anim:Bool){
        setProgress(pro, animated: anim, withDuration: 0.5)
    }
  
    func setProgress(_ pro:Int,animated anim:Bool,withDuration duration:Double){
      
        progress = pro
       
        CATransaction.begin()
        CATransaction.setDisableActions(!anim)
    CATransaction.setAnimationTimingFunction(CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeInEaseOut))
        CATransaction.setAnimationDuration(duration)

        progressLayer.strokeEnd = CGFloat(progress)/100.0
        CATransaction.commit()
      
    }
   
    fileprivate func angleToRadian(_ angle:Double)->CGFloat{
        return CGFloat(angle/Double(180.0) * M_PI)
    }

}
