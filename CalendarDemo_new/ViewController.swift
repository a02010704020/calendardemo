import UIKit

class ViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    
  
    @IBOutlet weak var calendar: UICollectionView!
    @IBOutlet weak var monthView: UIView!
    @IBOutlet weak var weekdayView: UIView!
    @IBOutlet weak var timeLabel: UILabel!
    var currentYear = Calendar.current.component(.year, from: Date())
    var currentMonth = Calendar.current.component(.month, from: Date())
    @IBAction func lastMonth(_ sender: UIButton) {
        currentMonth -= 1
        if currentMonth == 0{
            currentMonth = 12
            currentYear -= 1
        }
        setUp()
    }
    @IBAction func nextMonth(_ sender: UIButton) {
        currentMonth += 1
        if currentMonth == 13{
            currentMonth = 1
            currentYear += 1
        }
        setUp()
    }
    var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
    
    var numberOfDaysInThisMonth:Int{
        let dateComponents = DateComponents(year: currentYear ,
                                            month: currentMonth)

        let date = Calendar.current.date(from: dateComponents)!
        let range = Calendar.current.range(of: .day, in: .month,
                                           for: date)

        return range?.count ?? 0
    }
   
    var whatDayIsIt:Int{
        let dateComponents = DateComponents(year: currentYear ,
                                            month: currentMonth)
        let date = Calendar.current.date(from: dateComponents)!
        return Calendar.current.component(.weekday, from: date)
    }
  
    var howManyItemsShouldIAdd:Int{
        return whatDayIsIt - 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
  
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width / 7
        return CGSize(width: width, height: width)
    }
  
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
 
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return numberOfDaysInThisMonth + howManyItemsShouldIAdd
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath)
        if let textLabel = cell.contentView.subviews[2] as? UILabel{
            if indexPath.row < howManyItemsShouldIAdd {
                textLabel.text = ""
                if indexPath.row == 0 {
                    if let myProgress = cell.contentView.subviews[1] as? OPogressView{
                        myProgress.isHidden = true
                        myProgress.progress = 0
                    }
                }
                if let backView = cell.contentView.subviews[0] as? UIView{
                    backView.isHidden = true
                }
            }else{
                var daySteps = indexPath.row + 1 - howManyItemsShouldIAdd
                textLabel.text = "\(daySteps)"

                if let myProgress = cell.contentView.subviews[1] as? OPogressView{
                myProgress.progress = 0
                let number = Int.random(in: 0...100)
                myProgress.isHidden = false
                myProgress.setProgress(20, animated: true)
                }
                if let backView = cell.contentView.subviews[0] as? UIView{
                    backView.isHidden = false
                }
                if daySteps % 2 == 1 {
                    if let yellowView = cell.contentView.subviews[4] as? UIView{
                        yellowView.isHidden = true
                    }
                }else{
                    if let yellowView = cell.contentView.subviews[4] as? UIView{
                        yellowView.isHidden = false
                    }
                }
            }
        }
        return cell
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationBar.prefersLargeTitles = true
        setUp()
        
    }
    
    func setUp(){
        timeLabel.text = months[currentMonth - 1] + "  \(currentYear)"
        print(currentYear)
        print(currentMonth)
        print(whatDayIsIt)
        calendar.reloadData()
    }
    
    
}


